<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package THEMENEXT
 */

?>
	</div><!-- #content -->

	<footer>
       <div class="wrapper bottomsections">
           <div class="psection">
               <h3 class="psection__head"><?php the_field('footerfirsthead'); ?></h3>
               <ul class="psection__list">
                   <li class="psection__list--item"><a href="">Web Design</a></li>
                   <li class="psection__list--item"><a href="">Branding & Identity</a></li>
                   <li class="psection__list--item"><a href="">Mobile Design</a></li>
                   <li class="psection__list--item"><a href="">Print</a></li>
                   <li class="psection__list--item"><a href="">User Interface</a></li>
               </ul>
           </div>
           <div class="asection">
               <h2 class="asection__head"><?php the_field('footersecondhead'); ?></h2>
               <ul class="asection__list">
                   <li class="asection__list--item"><a href="">The Company</a></li>
                   <li class="asection__list--item"><a href="">History</a></li>
                   <li class="asection__list--item"><a href="">Vision</a></li>
               </ul>
           </div>
           <div class="gsection">
               <h2 class="gsection__head"><?php the_field('thirdsecondhead'); ?></h2>
               <ul class="gsection__list">
                   <li class="gsection__list--item"><a href="">Flickr</a></li>
                   <li class="gsection__list--item"><a href="">Picasa</a></li>
                   <li class="gsection__list--item"><a href="">iStockPhoto</a></li>
                   <li class="gsection__list--item"><a href="">PhotoDune</a></li>
               </ul>
           </div>
           <div class="csection">
               <h2 class="csection__head"><?php the_field('fourthhead'); ?></h2>
               <ul class="csection__list">
                   <li class="csection__list--item"><a href="">Basic Info</a></li>
                   <li class="csection__list--item"><a href="">Map</a></li>
                   <li class="csection__list--item"><a href="">Conctact Form</a></li>
               </ul>
           </div>
       </div>
       <div class="socialicos">
          <ul class="socialicos__list">
              <li class="socialicos__list--item"><a href="" class="fa fa-facebook-official"></a></li>
              <li class="socialicos__list--item"><a href="" class="fa fa-twitter-square"></a></li>
              <li class="socialicos__list--item"><a href="" class="fa fa-google-plus-square"></a></li>
              <li class="socialicos__list--item"><a href="" class="fa fa-linkedin-square"></a></li>
          </ul>
       </div>
       <p class="copyright">Copyright © 2015 Syed Miraj.All rights reserved.</p>
   </footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
