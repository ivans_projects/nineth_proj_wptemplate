<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package THEMENEXT
 */

get_header(); ?>


       <div class="updates">
      <a href="" class="updates__heading">updates & support</a>
      <div class="wrapper updatessupport">
        <div class="updatessupport__cont"> 
        <p class="updatessupport__cont--text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</p>
        <a href="" class="updatessupport__cont--btn">support center</a>
        </div>
        <img src="<?php the_field('updatescentreimg'); ?>" alt="updatesAndSuport">
    </div>
   </div>
   <div class="hireus">
      <div class="wrapper hireussec">
           <img src="<?php the_field('imagesectionhireus'); ?>" alt="hireusimg">
           <div class="hireus__text">
               <p class="hireus__text--head">need creative workers?</p>
               <a class="hireus__text--btn" href="">hire us</a>
           </div> 
       </div>
   </div>
<?php
get_sidebar();
get_footer();
