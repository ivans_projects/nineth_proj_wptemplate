<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package THEMENEXT
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	

 <div class="top">
     <div class="wrapper ">
      <header class="topstripe">
          <a href="" class="topstripe__logo"><img src="<?php the_field('logoimage'); ?>" alt="uniqueLogo"></a>
          <nav>
              <ul class="topstripe__list">
                  <li class="topstripe__list--item"><a href="">home</a></li>
                  <li class="topstripe__list--item"><a href="">services</a></li>
                  <li class="topstripe__list--item"><a href="">about</a></li>
                  <li class="topstripe__list--item"><a href="">portfolio</a></li>
                  <li class="topstripe__list--item"><a href="">academic</a></li>
                  <li class="topstripe__list--item"><a href="">blog</a></li>
                  <li class="topstripe__list--item"><a href="">contact us <br><span>(+966-322 322)</span></a></li>
              </ul>
          </nav>
      </header>
          <div class="toptext">
             <div class="text">
              <h2 class="text__heading">UNIQUE-TECH<br>APP DEVELOPMENT </h2>
              <p class="text__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. 
                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit. </p>
               <a href="#" class="text__btn">View More <span class="text__ico fa fa-plane"></span></a>
              </div>
               <div class="slideimg">
                <img src="<?php the_field('sliderimage'); ?>" alt="slideimg">
               </div>
          </div>

      </div>
  </div>

	<div id="content" class="site-content">
