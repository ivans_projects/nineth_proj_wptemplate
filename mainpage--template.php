<?php
/* Template Name: Page Content */ 
get_header(); ?>
    <div class="whatwedo">
      <div class="wrapper whatwedosections">
           <a href="" class="whatwedosections__btn">what we do</a>
           <div class="whatwedosections__section1">
               <img src="<?php the_field('what_we_do_image_1'); ?>" alt="monitor1">
               <div class="whatwedosections__content">
                   <h2 class="whatwedosections__content--head">web design</h2>
                   <p class="whatwedosections__content--firsttext">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                   <p class="whatwedosections__content--second">consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est. labo</p>
                   <a href="#" class="whatwedosections__content--btn">view more</a>
               </div>
           </div>
           <div class="whatwedosections__section2">
               <div class="whatwedosections__content">
                   <h2  class="whatwedosections__content--head">SEARCH ENGINE OPTIMIZATION</h2>
                   <p class="whatwedosections__content--firsttext"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                   <p class="whatwedosections__content--second">consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est. labo</p>
                   <a href="" class="whatwedosections__content--btn">view more</a>
               </div>
               <img src="<?php the_field('what_we_do_image_2'); ?>" alt="monitor1">
           </div>
           <div class="whatwedosections__section3">
               <img src="<?php the_field('what_we_do_image_3'); ?>" alt="monitor1">
               <div class="whatwedosections__content">
                   <h2 class="whatwedosections__content--head" >AFFILIATE MARKETTING</h2>
                   <p class="whatwedosections__content--firsttext">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                   <p class="whatwedosections__content--second">consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est. labo</p>
                   <a href="" class="whatwedosections__content--btn">view more</a>
               </div>
           </div> 
       </div>
   </div>
   <div class="howWeWork">
       <div class="howWeWork__content">
           <a class="howWeWork__content--btn" href="">how we work</a>
           <img class="howWeWork__content--image" src="<?php the_field('howweworkimage'); ?>" alt="howWeWork">
       </div>
   </div>
   <div class="features">
       <a class="features__btn" href="">our features</a>
      <div class="wrapper featuressectitems">
           <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Viusal Composer</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
             <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Responsive</h2>
               <p class="featuressect__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud</p>
           </div>
             <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Retina Redy</h2>
               <p class="featuressect__text">TLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
           </div>
           
       </div>
        <div class="wrapper featuressectitems">
              <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Typography</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
 <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Theme Options Panel</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
            <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">10000+ icons</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
            </div>
       <div class="wrapper featuressectitems">
      
             <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Dummy Content</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
             <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Many Style Available</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
             <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Header Style</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
       </div>
       <div class="wrapper featuressectitems">
            <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Support</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
            <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">Update</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>
           <div class="featuressect">
               <span class="fa fa-id-card-o featuressect__icos"></span><h2 class="featuressect__head">SEO Ready</h2>
               <p class="featuressect__text">TheFox comes with the Visual Composer Plugin. You won’t need to code or to 
    remember any shortcodes with our. </p>
           </div>

           </div>
   </div>
     <div class="packages wrapper">
       <a href="" class="packages__btn">our packages</a>
       <div class="packagessection">
           <div  class="packagessection__package1">
               <h2  class="packagessection__package1--head">Basic</h2>
               <ul class="packagessection__package1--list">
                   <li class="packagessection__package1--list__item"><a href="">5 web sites</a></li>
                   <li class="packagessection__package1--list__item"><a href="">15 e-mails</a></li>
                   <li class="packagessection__package1--list__item"><a href="">5 MySQL databases</a></li>
                   <li class="packagessection__package1--list__item"><a href="">10 GB</a></li>
                   <li class="packagessection__package1--list__item"><a href="">$5/ mo.</a></li>
               </ul>
               <a href="" class="packagessection__package1--btn">order now</a>
           </div>
             <div  class="packagessection__package2">
               <h2 class="packagessection__package2--head">Standard</h2>
               <ul  class="packagessection__package2--list">
                   <li class="packagessection__package2--list__item"><a href="">15 web sites</a></li>
                   <li class="packagessection__package2--list__item"><a href="">40 e-mails</a></li>
                   <li class="packagessection__package2--list__item"><a href="">20 MySQL databases</a></li>
                   <li class="packagessection__package2--list__item"><a href="">50 GB storage</a></li>
                   <li class="packagessection__package2--list__item"><a href="">$12 <span>/mo.</span></a></li>
               </ul>
               <a href="" class="packagessection__package2--btn">order now</a>
           </div>
             <div  class="packagessection__package3">
               <h2  class="packagessection__package3--head">Premium</h2>
               <ul class="packagessection__package3--list">
                   <li class="packagessection__package3--list__item"><a href="">30 web sites</a></li>
                   <li class="packagessection__package3--list__item"><a href="">50 e-mails</a></li>
                   <li class="packagessection__package3--list__item"><a href="">40 mySQL databases</a></li>
                   <li class="packagessection__package3--list__item"><a href="">100 GB storage</a></li>
                   <li class="packagessection__package3--list__item"><a href="">$19/ mo.</a></li>
               </ul>
               <a href="" class="packagessection__package3--btn">order now</a>
           </div>
       </div>
   </div>
   <div class="history">
      <div class="wrapper historytext">
          <div class="historytext__image">
            <img src="<?php the_field('ourstoryimage'); ?>" alt="">
           </div>
         <div class="historytext__imagetext">
            <h2  class="historytext__imagetext--heading">our history</h2>
            <p class="historytext__imagetext--text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</p>
            <p class="historytext__imagetext--text">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatisquasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur.</p>
            <a href="" class="historytext__imagetext--btn">browse our story</a>
           </div>
       </div>
   </div>
   <div class="projects">
      <a class="projects__btn" href="">OUR BUSINESS RELATED PROJECTS</a>
      <div class="wrapper projectssection">
            <div class="projectssection__items">
               <div class="projectssection__items--firstsect">
                <div>
                   <img src="<?php the_field('projimage1'); ?>" alt="technicalAid">
                   <p>technical aid</p>
               </div>
               <div>
                   <img src="<?php the_field('projimage2'); ?>" alt="technicalAid">
                   <p>technical aid</p>
               </div>
               <div>
                   <img src="<?php the_field('projimage3'); ?>" alt="technicalAid">
                   <p>technical aid</p>
               </div>
               <div>
                   <img src="<?php the_field('projimage4'); ?>" alt="technicalAid">
                   <p>technical aid</p>
               </div>
               </div>
               <div class="projectssection__items--secondsection">
               <div>
                   <img src="<?php the_field('projimage5'); ?>" alt="technicalAid">
                   <p>technical aid</p>
               </div>
               <div>
                   <img src="<?php the_field('projimage6'); ?>" alt="technicalAid">
                   <p>technical aid</p>
               </div>
               <div>
                   <img src="<?php the_field('projimage7'); ?>" alt="technicalAid">
                   <p>technical aid</p>
               </div>
               <div>
                   <img src="<?php the_field('projimage8'); ?>" alt="technicalAid">
                   <p>technical aid</p>
               </div>
                </div>
           </div>
       </div>
   </div>
   <a class="members__btn" href="">our team members</a>
   <div class="members">
      <div class="wrapper">
           <ul class="members__list">
               <li>
                <img src="<?php the_field('person1'); ?>" alt="firstPerson">
                    <div class="memberslist__text">
                    <strong class="memberslist__text--head"><?php the_field('person1head'); ?></strong><br>
                    <span class="memberslist__text--smalltext"><?php the_field('person1jobtitle'); ?></span>
                   </div>
               </li>
               <li><img src="<?php the_field('person2'); ?>" alt="firstPerson">
            <div class="memberslist__text">
                    <strong class="memberslist__text--head">penny huston</strong><br>
                    <span class="memberslist__text--smalltext">developer</span>
                   </div>
           </li>
               <li><img src="<?php the_field('person3'); ?>" alt="firstPerson">
            <div class="memberslist__text">
                    <strong class="memberslist__text--head">lennerd shelly</strong><br>
                    <span class="memberslist__text--smalltext">reviewer</span>
                   </div>
            </li>
               <li><img src="<?php the_field('person4'); ?>" alt="firstPerson">
                
                <div class="memberslist__text">
                    <strong class="memberslist__text--head">sheldon cupper</strong><br>
                    <span class="memberslist__text--smalltext">marketer</span>
                   </div>
                
            </li>
               <li><img src="<?php the_field('person5'); ?>" alt="firstPerson">
                <div class="memberslist__text">
                    <strong class="memberslist__text--head">ashley cole</strong><br>
                    <span class="memberslist__text--smalltext">managment</span>
                   </div>
            <li>
           </ul>
       </div>
   </div>
   <div class="updates">
      <a href="" class="updates__heading">updates & support</a>
      <div class="wrapper updatessupport">
        <div class="updatessupport__cont"> 
        <p class="updatessupport__cont--text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</p>
        <a href="" class="updatessupport__cont--btn">support center</a>
        </div>
        <img src="<?php the_field('updatescentreimg'); ?>" alt="updatesAndSuport">
    </div>
   </div>
   <div class="hireus">
      <div class="wrapper hireussec">
           <img src="<?php the_field('imagesectionhireus'); ?>" alt="hireusimg">
           <div class="hireus__text">
               <p class="hireus__text--head">need creative workers?</p>
               <a class="hireus__text--btn" href="">hire us</a>
           </div> 
       </div>
   </div>
   <?php get_footer(); ?>